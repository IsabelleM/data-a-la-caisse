source Caisse.sql

delete from product;

SELECT * FROM product;

-- Créer un produit “Truc”, avec comme description “Lorem Ipsum” comme image “https://picsum.photos/200”, qui coute 200 €.
INSERT INTO product VALUES (NULL, "Truc", "Lorem Ipsum", "https://fastly.picsum.", 200, True);

-- Créer un produit “Bidule”, avec comme description “Lorem merol” comme image “https://picsum.photos/100”, qui coute 500 €.
INSERT INTO product VALUES (NULL, "Bidule", "Lorem Merol", "https://fastly.picsum.", 500, True);

-- Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.
SELECT * FROM product;

-- Récupérer Truc à partir de son id et vérifier sa description et son image.
SELECT description, images FROM product WHERE product_id = 1;

-- Créer une nouvelle commande. Ajouter 3 Trucs à la commande.
INSERT INTO `order` VALUES (NULL, NULL, NULL, NULL, 'cart', 'credit_card');
select * FROM `order`;
INSERT INTO order_line VALUES (1,1,3);

-- Ajouter 1 Bidule à la commande.
INSERT INTO order_line VALUES (1, 2, 1);

-- Créer le client “Bob” avec comme mail “bob@gmail.com”. Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.
INSERT INTO client VALUES (NULL, "Bob", "bob@gmail.com", "0123456789", NULL);
SELECT * FROM client;

-- Essayer de passer la commande dans l'état suivant (vérifier qu'il y a une erreur).
SELECT * FROM `order`;

-- Associer Bob à la commande
UPDATE `order` SET client_id = 1 WHERE order_id = 1;

-- Créer l’adresse “12 street St, 12345 Schenectady, New York, US”
INSERT INTO address VALUES (NULL, "12 street St", "Schenectady", 12345, "New-York", "US");
SELECT * FROM address;

-- Essayer d’associer cette adresse comme facturation à la commande.
SET @adresse_facturation_id = (SELECT address_id FROM `address` WHERE street = '12 street St' AND city = 'Schenectady');
UPDATE `order` 
JOIN `client` ON `order`.client_id = `client`.client_id
SET `order`.billing_address_id = @adresse_facturation_id
WHERE `client`.name = 'Bob';

-- Associer cette adresse à Bob.
SET @nouvelle_adresse_id = LAST_INSERT_ID();

-- Insérez l'association entre le client "BoB" et la nouvelle adresse
select @nouvelle_adresse_id;
INSERT INTO `client_address` (client_id, address_id)
VALUES (1, @nouvelle_adresse_id);

-- Associer cette adresse comme facturation à la commande.
UPDATE `order`
SET billing_address_id = 1
WHERE order_id = 1;

-- Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
INSERT INTO address VALUES (NULL, "12, boulevard de Strasbourg", "Toulouse", 31000, "Occitanie", "France");
SET @nouvelle_adresse_id = last_insert_id();
SELECT * FROM address;

-- Associer cette adresse à Bob.
select * from client;
INSERT INTO `client_address` (client_id, address_id)
VALUES (1, @nouvelle_adresse_id);

-- Associer cette adresse comme adresse de livraison.
UPDATE `order`
SET delivery_address_id = 1
WHERE order_id = 1;

-- Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
SELECT * FROM `order`;

-- Associer le moyen de paiement “Card” à la commande.
UPDATE `order`
SET payment_method = 'credit_card'               
WHERE order_id = 1;  

-- Passer la commande dans l’état suivant. 
UPDATE `order` SET order_state = 'sent' WHERE order_id = 1;

-- -- Vérifier que la commande est dans l'état sent
SELECT * FROM `order` WHERE order_id = 1 AND order_state = 'sent'; 

-- Récupérer la liste des adresses de Bob. -- erreur il me donne une seule addresse
SELECT *
FROM `address`
JOIN client_address ON `address`.address_id = client_address.address_id
JOIN `client` ON client_address.client_id = `client`.client_id
WHERE `client`.name = 'Bob';

-- Récupérer la liste des commandes de Bob.
SELECT *
FROM `order`
JOIN `client` ON `order`.client_id = `client`.client_id
WHERE `client`.name = 'Bob';
