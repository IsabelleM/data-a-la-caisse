from pydantic import BaseModel


class Product(BaseModel):
    id: int | None = None
    name: str | None = None
    description: str | None = None
    images: str | None = None
    price: float | None = None
    available: bool = True

class Order(BaseModel):
    id: int | None = None
    client: int | None = None
    delivery_address_id: int | None = None
    billing_address_id: int | None = None
    order_state: str
    payment_method: str

class Client(BaseModel):
    id: int | None = None
    name: str | None = None
    email: str | None = None
    phone: str | None = None
    default_address_id: int | None = None

class Address(BaseModel):
    id: int | None = None
    street: str | None = None
    city: str | None = None
    zipcode: int | None = None
    state: str | None = None
    country: str | None = None

class order_line(BaseModel):
    order_id: int | None = None
    product_id: int | None = None
    qt: int | None = None