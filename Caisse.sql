drop DATABASE Caisse;
CREATE DATABASE IF NOT EXISTS `Caisse`;

USE Caisse;

CREATE TABLE `address` (
  address_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  street VARCHAR(255),
  city VARCHAR(255),
  zipcode INT,
  state VARCHAR(255),
  country VARCHAR(255)
);

CREATE TABLE `client` (
  client_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(50),
  email VARCHAR(56),
  phone VARCHAR(255),
  default_address_id INT,
  FOREIGN KEY (default_address_id) REFERENCES `address` (address_id)
);

CREATE TABLE `order` (
  order_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  client_id INT,
  delivery_address_id INT,
  billing_address_id INT,
  order_state ENUM('cart', 'validated', 'sent', 'delivered'),
  payment_method ENUM('credit_card', 'bank_check', 'cash'),
  FOREIGN KEY (client_id) REFERENCES `client` (client_id),
  FOREIGN KEY (delivery_address_id) REFERENCES `address` (address_id),
  FOREIGN KEY (billing_address_id) REFERENCES `address` (address_id)
);

CREATE TABLE `client_address` (
  client_id INT,
  address_id INT,
  UNIQUE(client_id, address_id),
  FOREIGN KEY (client_id) REFERENCES `client` (client_id) on delete CASCADE,
  FOREIGN KEY (address_id) REFERENCES `address` (address_id) on delete CASCADE
);

CREATE TABLE `product` (
  product_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(255),
  description TEXT,
  images TEXT,
  price FLOAT,
  available BOOL
);

CREATE TABLE `order_line` (
  order_id INT,
  product_id INT,
  qt INT,
  FOREIGN KEY (order_id) REFERENCES `order` (order_id) on delete CASCADE,
  FOREIGN KEY (product_id) REFERENCES `product` (product_id) on delete CASCADE
);

-- CREATE TABLE `payment_method` (
--   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
--   credit_card BOOL,
--   bank_check_on_delivery BOOL,
--   cash_on_delivery BOOL
-- );

-- CREATE TABLE `order_state` (
--   id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
--   cart BOOL,
--   validated BOOL,
--   sent BOOL,
--   delivered BOOL
-- );

