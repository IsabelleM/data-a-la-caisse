from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from classes import Product, Order, Client, Address
import mysql.connector


app = FastAPI()

mydb = mysql.connector.connect(
    host="localhost",
    user="david",
    password="coucou123",
    database="Caisse"
)

@app.get(":")
def read_root():
    return {"message": "hello"}


#Produit
@app.get("/product")
def get_product():
    with mydb.cursor(buffered=True) as cursor:
        cursor.execute("SELECT * FROM product")
    return cursor.fetchall()

@app.post("/product")
def add_product():
    with mydb.cursor() as cursor:
        cursor.execute("INSERT INTO product VALUES (NULL, 'Truc', 'Lorem Ipsum', 'https://fastly.picsum.', 200, True);"),
    return cursor.fetchall()

@app.post("/product")
def add_product():
    with mydb.cursor() as cursor:
        cursor.execute("INSERT INTO product VALUES (NULL, 'Bidule', 'Lorem Merol', 'https://fastly.picsum.', 500, True);"),
    return cursor.fetchall()
     

@app.post("/product")
async def create_product(product: Product):
    mycursor = mydb.cursor()
    sql = "insert into product(name) values (%s);"
    val = (product.name,)
    mycursor.execute(sql, val)
    mydb.commit()
    product = mycursor.lastrowid
    return product

@app.get("/product/{id}")
async def get_product_by_id(id: int) -> Product:
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "select * from product where product_id=%s;"
    mycursor.execute(sql, [id])
    data_product = mycursor.fetchall()[0]
    un_produit = Product()
    un_produit.id = int(data_product["product_id"])
    un_produit.name = data_product["name"]
    un_produit.description = data_product ["description"]
    un_produit.images = data_product["images"]
    un_produit.price = data_product["price"]
    return un_produit


#Client
@app.get("/client")
def get_clients():
    with mydb.cursor(buffered=True) as cursor:
        cursor.execute("SELECT * FROM client")
        result = cursor.fetchall()
    return result

@app.post("/client")
async def create_client(client: Client):
    mycursor = mydb.cursor()
    sql = "insert into client(name) values (%s);"
    val = (client.name,)
    mycursor.execute(sql, val)
    mydb.commit()
    client = mycursor.lastrowid
    return client

@app.get("/client")
async def create_client() -> list:
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "select * from client;"
    mycursor.execute(sql)
    liste_de_client = mycursor.fetchall()
    return liste_de_client

@app.get("/client/{id}")
async def get_client_by_id(id: int) -> Client:
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "select * from client where client_id=%s;"
    mycursor.execute(sql, [id])
    data_client = mycursor.fetchall()[0]
    un_client = Client()
    un_client.id = int(data_client["client_id"])
    un_client.name = data_client["name"]
    un_client.email = data_client["email"]
    un_client.phone = data_client["phone"]
    un_client.default_address_id = int(data_client["default_address_id"])
    return un_client

@app.get("/address")
async def create_adresse_facturation() -> list:
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "INSERT INTO address VALUES (NULL, '12 street St', 'Schenectady', 12345, 'New-York', 'US');"
    mycursor.execute(sql)
    adresse_facturation = mycursor.fetchall()
    return adresse_facturation

@app.get("/address")
async def create_adresse_livraison() -> list:
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "INSERT INTO address VALUES (NULL, 12, boulevard de Strasbourg, Toulouse, 31000, Occitanie, France);"
    mycursor.execute(sql)
    adresse_livraison = mycursor.fetchall()
    return adresse_livraison

# Attribuer les adresses au client
@app.put("/assign_addresses_to_client/{client_id}/{client_name}")
def assign_adresses_to_client(client_id: int, client_name: str):
    # Vérifier si le client existe
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    query_check_client = "SELECT * FROM client WHERE name = %s"
    data_check_client = (client_name,)
    mycursor.execute(query_check_client)
    mydb.execute(query_check_client, data_check_client)
    client = mydb.fetchone()
    query_assign_adresses = "UPDATE `adresses` SET client_id = %s WHERE client_id = %s"
    data_assign_adresses = (client["client_id"], client_id)
    mycursor.execute(query_assign_adresses)
    mydb.execute(query_assign_adresses, data_assign_adresses)
    mydb.commit()
    adresses = mycursor.fetchall()
    return adresses

#Order
@app.get("order/")
def get_order():
    with mydb.cursor(buffered=True) as cursor:
        cursor.execute("SELECT * FROM order")
        order = Order()
        order = cursor.lastrowid()
    return order

@app.put("/order")
def create_order():
    # Code pour créer une commande dans la base de données
    query = "INSERT INTO `order` VALUES ()"
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    mycursor.execute(query)
    commande = mycursor.lastrowid
    return commande

@app.get("/order")
async def get_order():
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "SELECT * FROM `order`;"
    mycursor.execute(sql)
    list_cmd = mycursor.fetchall()
    return list_cmd

@app.get("/order/{id}")
async def get_order_by_id(id: int) -> Order:
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "select * from client where client_id=%s;"
    mycursor.execute(sql, [id])
    data_order = mycursor.fetchall()[0]
    une_commande = Order()
    une_commande.id = int(data_order["order_id"])
    return une_commande

#Attribuer une commande à un client:
@app.put("/assign_order_to_client/{order_id}/{client_name}")
def assign_order_to_client(order_id: int, client_name: str):
    # Vérifier si le client existe
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    query_check_client = "SELECT * FROM client WHERE name = %s"
    data_check_client = (client_name,)
    mycursor.execute(query_check_client)
    mydb.execute(query_check_client, data_check_client)
    client = mydb.fetchone()

    # Attribuer la commande au client
    query_assign_order = "UPDATE `order` SET client_id = %s WHERE order_id = %s"
    data_assign_order = (client["client_id"], order_id)
    mycursor.execute(query_assign_order)
    mydb.execute(query_assign_order, data_assign_order)
    mydb.commit()
    commande = mycursor.fetchall()
    return commande

#Ajouter 3 Trucs à la commande
@app.put("/order")
async def add_product():
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "INSERT INTO order_line VALUES (1,1,3);"
    mycursor.execute(sql)
    ajout_truc = mycursor.fetchall()
    return ajout_truc

#Ajouter 1 Bidule à la commande
@app.put("/order")
async def add_product():
    mycursor = mydb.cursor(dictionary=True, buffered=True)
    sql = "INSERT INTO order_line VALUES (1, 2, 1);"
    mycursor.execute(sql)
    mydb.commit()
    ajout_bidule = mycursor.fetchall()
    return ajout_bidule

# Essayer d’associer cette adresse comme facturation à la commande.
@app.put("/order")
def adress_to_facturation():
    with mydb.cursor() as cursor:
        cursor.execute(""" UPDATE `order` 
        JOIN `client` ON `order`.client_id = `client`.client_id
        SET `order`.billing_address_id = @adresse_facturation_id
        WHERE `client`.name = 'Bob' """)
        result = mydb.commit()
    return result


@app.post("/adress")
def get_address():
    with mydb.cursor(dictionary=True) as cursor:
        cursor.execute("SELECT * FROM address")
        result = cursor.fetchall()
    return result 

#if __name__ == '__main__':
